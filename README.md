# deployctl Build image

## purpose

This project creates a CentOS docker Image to build the [deployctl](https://gitlab.com/deployctl/deployctl) project.

Uses [gioxa/imagebuilder-c7](https://microbadger.com/images/gioxa/imagebuilder-c7) and builds with `odagrun` on `openshift online starter` as **non-root** and **non-privileged**.

## installed Packages

- ca-certificates 
- lcov 
- util-linux 

### development packages:

- zlib-devel
- libcurl-devel
- libcjson-devel
- libyaml-devel
- rpm-devel 
- libzip-devel
- openssl-devel
- libgit2-devel
- check-devel
- libdb-devel
- cmark-devel


### Group install:

- @Development Tools


----

*Build with [odagrun](https://www.odagrun.com) on openshift-online-starter*

